const express = require("express");
const cors = require("cors");
// ROUTER
const authRoter = require("./routes/authRoutes");
const studentRoutes = require("./routes/studentRoutes");
const coursRoutes = require("./routes/coursesRoutes");
const adminRoutes = require("./routes/adminRouter")
// UTILTS
const userRole = require("./utilts/userENUM")
// IMPORT MIDDLEAWERE
const authMiddleawere = require("./middlewares/authTokenHeaders")
const authCheck = require("./middlewares/authorizationCheck")
// ERROR HANDLER
const ErrorClass = require("./utilts/ErrorClass");
const errorController = require("./controllers/errorControllers");
const app = express();
// MIDLWARE
app.use(express.json());
app.use(cors());

app.use("/api/v1/auth", authRoter);
app.use("/api/v1/courses", authMiddleawere, authCheck.check_SUPER_ADMIN_AND_ADMIN, coursRoutes);
app.use("/api/v1/students", authMiddleawere, authCheck.check_SUPER_ADMIN_AND_ADMIN ,studentRoutes);
app.use("/api/v1/admin", authMiddleawere, authCheck.check_SUPER_ADMIN, adminRoutes);
app.all("*", (req, res, next) => {
  next(new ErrorClass(`${req.path} path not fount`, 404));
});

// ERROR CONTROLLER
app.use(errorController);

module.exports = app;
