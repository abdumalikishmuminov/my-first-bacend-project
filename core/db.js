const { Sequelize } = require("sequelize")

const env = process.env

const configDB = {
    host: "localhost",
    port: 5432,
    username: "postgres",
    password: "root",
    dialect: "postgres",
    database: "education"
}

const sequelize = new Sequelize(configDB)

module.exports = sequelize