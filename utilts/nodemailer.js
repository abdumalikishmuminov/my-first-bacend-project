const nodemailer = require("nodemailer")


const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: process.env.EMAIL,
        pass: process.env.EMAIL_CODE
    }
});

const sendMailFn = (to, verifyCode, subjectText) => {

    const mailOptions = {
        from: process.env.EMAIL,
        to: to,
        subject: subjectText,
        html: verifyCode
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });

}


module.exports = sendMailFn
