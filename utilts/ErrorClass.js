class ErrorClass extends Error {
    constructor(message, statusCode) {
        super()
        this.status = `${statusCode}`.startsWith(4) ? "fail" : "error"
        this.statusCode = statusCode;
        this.message = message;
        this.isOperational = true
        Error.captureStackTrace(this, this.constructor)
    }

}

module.exports = ErrorClass