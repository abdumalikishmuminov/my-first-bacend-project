const jwt = require("jsonwebtoken")

const generatorToken = (payload, jwtSecret, option) => {
    return new Promise((resolve, reject) => {
        jwt.sign(payload, jwtSecret, option, (err, token) => {
            if (err) {
                reject(err)
            } else {
                resolve(token)
            }
        })
    })
}

module.exports = generatorToken