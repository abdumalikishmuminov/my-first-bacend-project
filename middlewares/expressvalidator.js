const { body } = require("express-validator")
exports.studentExpressValidator = [body("firstName").notEmpty().withMessage("Bo`sh bo`lmasligi kerak").isLength({ min: 3 }).withMessage("Kamida 3 ta harfdan kam bo`lmasligi kerak").isLength({ max: 30 }).withMessage("siz ko`p ma`lumot kiritib yubordingiz"),
body("lastName")
    .notEmpty().withMessage("Bo`sh bo`lmasligi kerak")
    .isLength({ min: 3 }).withMessage("Kamida 3 ta harfdan kam bo`lmasligi kerak")
    .isLength({ max: 30 }).withMessage("siz ko`p ma`lumot kiritib yubordingiz"),
body("birthDay")
    .notEmpty().withMessage("Bo`sh bo`lmasligi kerak")
    .isAfter("1940-01-01").withMessage("siz 1939 yilda tug`ilganmisiz?")
    .isBefore("2018-12-31").withMessage("Sizning yoshingiz shunchalik kichkinami?")
]

exports.coursExpressValidator = [body("name")
    .notEmpty().withMessage("bo`sh bo`lmasligi kerak")
    .isLength({ min: 5 }).withMessage("Kamida 5 ta harfdan iborat bo`lishi kerak")]

exports.registerExpressValidator = [
    body("firstName")
        .notEmpty().withMessage("Foydalanuvchi ismi bo`sh bo`lmasligi kerak")
        .isLength({ min: 3 }).withMessage("Kamida 3 ta simvoldan iborat bo`lishi kerak")
        .isLength({ max: 15 }).withMessage("Ko`pi bilan 15 ta simvoldan iborat bo`lishi kerak"),
    body("username")
        .notEmpty().withMessage("Foydalanuvchi nomi bo`sh bo`lmasligi kerak")
        .isLength({ min: 3 }).withMessage("Kamida 3 ta simvoldan iborat bo`lishi kerak")
        .isLength({ max: 20 }).withMessage("Ko`pi bilan 15 ta simvoldan iborat bo`lishi kerak"),
    body("password")
        .notEmpty().withMessage("parol bo`sh bo`lmasligi kerak")
        .isLength({ min: 6 }).withMessage("Kamida 6 ta simvoldan iborat bo`lishi kerak")
        .isLength({ max: 20 }).withMessage("Ko`pi bilan 20 ta simvoldan iborat bo`lishi kerak"),
    body("email")
        .notEmpty().withMessage("email bo`sh bo`lmasligi kerak")
        .isEmail().withMessage("kiritilgan ma`lumot email emas"),
    body("phoneNumber")
    .notEmpty().withMessage("tel nomerizni kiriting")
]
exports.loginExpressValidator = [
    body("username")
        .notEmpty().withMessage("Foydalanuvchi nomi bo`sh bo`lmasligi kerak")
        .isLength({ min: 3 }).withMessage("Kamida 3 ta simvoldan iborat bo`lishi kerak")
        .isLength({ max: 20 }).withMessage("Ko`pi bilan 15 ta simvoldan iborat bo`lishi kerak"),
    body("password")
        .notEmpty().withMessage("parol bo`sh bo`lmasligi kerak")
        .isLength({ min: 6 }).withMessage("Kamida 6 ta simvoldan iborat bo`lishi kerak")
        .isLength({ max: 20 }).withMessage("Ko`pi bilan 20 ta simvoldan iborat bo`lishi kerak"),
]