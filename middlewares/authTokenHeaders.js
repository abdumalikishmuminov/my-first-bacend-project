const { verify } = require("jsonwebtoken")
const ErrorClass = require("../utilts/ErrorClass")

module.exports = (req, res, next) => {
    const authHeader = req.headers.authorization
    if (!authHeader) {
        return next(new ErrorClass("Avtorizatsiyadan o`tingggggggggggggggggg", 401))
    }
    const token = authHeader.slice(7)
    const user = verify(token, process.env.JWT_SECRET_CODE)
    if (!user) {
        return next(new ErrorClass("Avtorizatisadan o`ting", 401))
    }
    req.user = user
    next()
}