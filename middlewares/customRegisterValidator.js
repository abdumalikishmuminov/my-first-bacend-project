const ErrorClass = require("../utilts/ErrorClass")
const catchAsync = require("../utilts/errorHandlerInAsync")
const userENUM = require("../utilts/userENUM")
const customRegisterValidator = catchAsync(async (req, res, next) => {
    const { email, phoneNumber, userRole } = req.body
    const sabachka = email.split("").filter(e => e === "@")
    if (sabachka.length !== 1) {
        return next(new ErrorClass("Email noto`g`ri kiritilgan", 400))
    }

    const tel = !phoneNumber.startsWith("+998")
    if (tel) {
        return next(new ErrorClass("Telefon nomer notog`ri kitilgan", 400))
    }
    next()
})
module.exports = customRegisterValidator