const userRole = require("../utilts/userENUM")
const ErrorClass = require("../utilts/ErrorClass")

exports.check_SUPER_ADMIN_AND_ADMIN = (req, res, next) => {
    if (req.user.role !== userRole.SUPER_ADMIN || req.user.role !== userRole.ADMIN) {
        return next(new ErrorClass("Siz bu yo`lga ruxsat yo`q", 401))
    }
    next()
};

exports.check_SUPER_ADMIN = (req, res, next) => {
    if (req.user.role !== userRole.SUPER_ADMIN) {
        return next(new ErrorClass("Siz bu yo`lga ruxsat yo`q", 401))
    }
    next()
};