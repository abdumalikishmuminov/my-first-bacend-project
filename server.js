require("dotenv").config()
const db = require("./core/db")
const app = require("./app")

const PORT = process.env.PORT || 1994

const start = async () => {
    try {
        await db.authenticate()
        console.log("Database Connection")
        db.sync()
        app.listen(PORT, () => { console.log(`Server ${process.env.NODE_ENV} started port on ${PORT}`) })

    }
    catch (error) {
        console.log(error)
        process.exit(1)
    }
}
start()
