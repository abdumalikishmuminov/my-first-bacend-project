const express = require("express")
const studentControllers = require("../controllers/studentsControllers")
const { studentExpressValidator } = require("../middlewares/expressvalidator")

const router = express.Router()

router
    .route("/")
    .get(studentControllers.allStudents)
    .post(studentExpressValidator, studentControllers.createStudents)
router
    .route("/:id")
    .get(studentControllers.getStudentById)
    .put(studentExpressValidator, studentControllers.updateStudent)
    .delete(studentControllers.deleteStudent)
module.exports = router