const express = require("express")
const adminController = require("../controllers/adminControllers")
const expressValidator = require("../middlewares/expressvalidator")
const router = express.Router()


router
    .route("/")
    .get(adminController.getUsers)
    .post(expressValidator.registerExpressValidator, adminController.createAdmin);

router
    .route("/:id")
    .patch(expressValidator.registerExpressValidator, adminController.updateAdmin)
    .delete(adminController.deleteAdmin);


module.exports = router