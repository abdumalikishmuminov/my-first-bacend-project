const express = require("express")
const authControllers = require("../controllers/authControllers")
const expressvalidator = require("../middlewares/expressvalidator")
const customRegisterValidator = require("../middlewares/customRegisterValidator")

const router = express.Router()

router.post("/register", expressvalidator.registerExpressValidator,
    customRegisterValidator,
    authControllers.register)

router.get("/verify/:verifyCode", authControllers.verify)

router.post("/login", expressvalidator.loginExpressValidator, authControllers.login)
module.exports = router