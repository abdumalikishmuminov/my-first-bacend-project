const express = require("express")
const coursControllers = require("../controllers/coursesControllers")
const { coursExpressValidator } = require("../middlewares/expressvalidator")
const authUser = require("../middlewares/authTokenHeaders")

const router = express.Router()

router
    .route("/")
    .get(coursControllers.allCourses)
    .post(coursExpressValidator, coursControllers.createCours)
router
    .route("/:id")
    .get(coursControllers.getCoursById)
    .put(coursExpressValidator, coursControllers.updateCours)
    .delete(coursControllers.deleteCours)
router
    .route("/:id/student")
    .get(coursControllers.getCourseStudents)
module.exports = router