const { DataTypes, UUIDV4 } = require("sequelize")
const sequelize = require("../core/db")
const userENUM = require("../utilts/userENUM")
const { hash } = require("bcryptjs")

const User = sequelize.define("users", {
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: UUIDV4
    },
    firstName: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            min: { args: [3], msg: "Foydalanuvchi ismi kamida 3 ta simvoldan iborat bo`lishi kerak" },
            max: { args: [15], msg: "Foydalanuvchi ismi ko`pi 6 ta simvoldan iborat bo`lishi kerak" }
        }
    },
    lastName: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            min: { args: [3], msg: "Foydalanuvchi familyasi kamida 3 ta simvoldan iborat bo`lishi kerak" },
            max: { args: [15], msg: "Foydalanuvchi familyasi ko`pi 6 ta simvoldan iborat bo`lishi kerak" }
        }
    },
    phoneNumber: {
        type: DataTypes.STRING,
        allowNull: false,
        // unique: true,
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        // unique: true,
        validate: { isEmail: true }
    },
    username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
            min: { args: [3], msg: "Foydalanuvchi nomi kamida 3 ta simvoldan iborat bo`lishi kerak" },
            max: { args: [20], msg: "Foydalanuvchi nomi ko`pi 6 ta simvoldan iborat bo`lishi kerak" }
        }
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            min: { args: [6], msg: "Parol kamida 6 ta simvoldan iborat bo`lishi kerak" },
            max: { args: [20], msg: "Parol ko`pi bilan 20 ta simvol bo`lishi kerak" }
        }
    },
    userRole: {
        type: DataTypes.ENUM(Object.values(userENUM)),
        allowNull: false,
        defaultValue: "ADMIN",
    },
    verificationCode: {
        type: DataTypes.UUID,
        allowNull: false,
        defaultValue: UUIDV4
    },
    isVerified: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
    },
}, {
    underscored: true, timestamps: true, hooks: {
        async beforeCreate(user) {
            user.password = await hash(user.password, 8)
        }
    }
})

module.exports = User