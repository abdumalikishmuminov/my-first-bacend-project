const { DataTypes } = require("sequelize")
const sequelize = require("../core/db")
const StudentModel = require("./StudentModel")
const Courses = sequelize.define("courses", {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER,

    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            min: { args: [5], msg: "Kursni nomi kamida 5ta harfdan iborat bo`lishi kerak" }
        }
    },
    discription: DataTypes.TEXT,

}, {
    underscored: true
})
Courses.hasMany(StudentModel, { as: "student", foreignKey: "coursId" })
StudentModel.belongsTo(Courses, { as: "cours", onDelete: "cascade" })
module.exports = Courses