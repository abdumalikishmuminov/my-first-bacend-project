const { DataTypes } = require("sequelize")
const sequelize = require("../core/db")

const Students = sequelize.define("students", {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER,
    },
    firstName: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            min: { args: [3], msg: "kamida 3 ta harfdan kam bo`lmasligi kerak" },
            max: { args: [30], msg: "ko`pi bilan 30 ta harfdan oshmasligi kerak" }
        }
    },
    lastName: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            min: { args: [3], msg: "kamida 3 ta harfdan kam bo`lmasligi kerak" },
            max: { args: [30], msg: "ko`pi bilan 30 ta harfdan oshmasligi kerak" }
        }
    },
    birthDay: {
        type: DataTypes.DATEONLY,
        validate: {
            isAfter: { args: "1940-01-01", msg: "Kamida 1940 yildan kattalar ro`yhatga yo`zilishi mumkin" },
            isBefore: { args: "2018-12-31", msg: "2018 yildan keyin tug`ilganlar ro`yhatga yozilolmaydi" }
        }
    }
}, { underscored: true, })


module.exports = Students