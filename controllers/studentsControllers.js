const StudentModel = require("../models/StudentModel");
const { validationResult } = require("express-validator");
const errorAsync = require("../utilts/errorHandlerInAsync");
const ErrorClass = require("../utilts/ErrorClass");
const CoursesModel = require("../models/CoursesModel");
const { Op } = require("sequelize");

exports.allStudents = errorAsync(async (req, res, next) => {
  const { size = 2, page = 1, search = null } = req.query;

  const getAllStudents = await StudentModel.findAndCountAll({
    limit: size,
    offset: (page - 1) * size,
    where: search && {
      [Op.or]: [
        { firstName: { [Op.iLike]: `%${search}%` } },
        { lastName: { [Op.iLike]: `%${search}%` } },
      ],
    },
  });
  getAllStudents.page = Math.ceil(getAllStudents.count / size);
  res.status(200).json({
    status: "succesfull",
    message: "Barcha studentlar",
    error: null,
    content: getAllStudents.rows,
    pagination: {
      allPages: getAllStudents.page,
      totalItems: getAllStudents.count,
      isLastPage: getAllStudents.page === +page,
      isfirstPage: +page - 1 === 0,
      page,
    },
  });
});

exports.getStudentById = errorAsync(async (req, res, next) => {
  const { id } = req.params;
  const studentById = await StudentModel.findByPk(id, {
    include: { model: CoursesModel, as: "cours" },
  });
  if (!studentById) {
    return next(new ErrorClass(`${id} not found for Student`, 404));
  }
  res.status(200).json({
    status: "succesfull",
    message: "Student haqida to`liq ma`lumot",
    error: "",
    data: { studentById },
  });
});

exports.createStudents = errorAsync(async (req, res, next) => {
  const validateErr = validationResult(req);
  if (!validateErr.isEmpty()) {
    let err = new ErrorClass("Validation Error", 400);
    err.errors = validateErr.errors;
    return next(err);
  }
  const newStudent = await StudentModel.create(req.body);
  res.status(201).json({
    status: "succes",
    message: "yangi student qo`shildi",
    data: { newStudent },
  });
});

exports.updateStudent = errorAsync(async (req, res, next) => {
  const { id } = req.params;
  const validateErr = validationResult(req);
  if (!validateErr.isEmpty()) {
    let err = new ErrorClass("Validation Error", 400);
    err.isOperational = true;
    err.errors = validateErr.errors;
    return next(err);
  }
  const studentById = await StudentModel.findByPk(id);
  if (!studentById) {
    return next(new ErrorClass(`${id} not found for Student`, 404));
  }
  res.status(203).json({
    status: "succes",
    message: "student ma`lumotlari yangilandi",
    data: { studentById },
  });
});

exports.deleteStudent = errorAsync(async (req, res, next) => {
  const { id } = req.params;
  const studentById = await StudentModel.findByPk(id);
  if (!studentById) {
    return next(new ErrorClass(`${id} not found for Student`, 404));
  }
  await studentById.destroy();
  res.status(204).json({
    status: "succes",
    message: "student ma`lumotlari o`chirildi",
  });
});
