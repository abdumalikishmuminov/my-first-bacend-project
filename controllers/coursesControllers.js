const StudentModel = require("../models/StudentModel")
const { validationResult } = require("express-validator")
const { Op } = require("sequelize")
const errorAsync = require("../utilts/errorHandlerInAsync")
const ErrorClass = require("../utilts/ErrorClass")
const CoursModel = require("../models/CoursesModel")


exports.allCourses = errorAsync(async (req, res, next) => {
    const { size = 2, page = 1, search = null } = req.query
    const getCourses = await CoursModel.findAndCountAll({
        limit: size,
        offset: (page - 1) * size,
        where: search && 
        {[Op.or]: 
            [
                {name: {[Op.iLike]: `%${search}%`}},
                {discription: {[Op.iLike]: `%${search}%`}}
        ]}     
    }
    )


    getCourses.page = Math.ceil(getCourses.count / size)
    if (!(+page <= getCourses.page) || !(+size <= getCourses.count)) {
        return next(new ErrorClass(`Ushbu so'ralgan ma'lumotlar (sahifa:${page}, ma'lumotlar soni:${size}) topilmadi`, 404))
    }
    res.status(200).json({
        status: "succesfull",
        message: "Barcha kurslar",
        error: null,
        content: getCourses.rows,
        pagenation: {
            allPages: getCourses.page,
            totalItems: getCourses.count,
            isLastPage: getCourses.page === +page,
            isFirstPage: (+page - 1) === 0,
            hasNextPage: getCourses.page > +page,
            page: +page
        }
    })
})

exports.getCoursById = errorAsync(async (req, res, next) => {
    const { id } = req.params
    const coursById = await CoursModel.findByPk(id)
    if (!coursById) {
        return next(new ErrorClass(`${id} not found for Cours`, 404))
    }
    res.status(200).json({
        status: "succesfull",
        message: `${coursById.name} kursi haqida to\`liq ma\`lumot`,
        error: "",
        data: {
            coursById
        }
    })
})

exports.createCours = errorAsync(async (req, res, next) => {
    const validateErr = validationResult(req)
    if (!validateErr.isEmpty()) {
        let err = new ErrorClass("Validation Error", 400)
        err.errors = validateErr.errors
        err.isOperational = true
        return next(err)
    }
    const newCours = await CoursModel.create(req.body)
    res.status(201).json({
        status: "succes",
        message: "yangi kurs qo`shildi",
        data: { newCours }
    })
})

exports.updateCours = errorAsync(async (req, res, next) => {
    const { id } = req.params
    const { name, discription } = req.body
    const validateErr = validationResult(req)
    if (!validateErr.isEmpty()) {
        let err = new ErrorClass("Validation Error", 400)
        err.errors = validateErr.errors
        err.isOperational = true
        return next(err)
    }
    const coursById = await CoursModel.findByPk(id)
    if (!coursById) {
        return next(new ErrorClass(`${id} not found for cours`, 404))
    }
    coursById.name = name;
    coursById.discription = discription
    coursById.save()

    res.status(203).json({
        status: "succes",
        message: "kurs ma`lumotlari yangilandi",
        data: { coursById }
    })
})

exports.getCourseStudents = errorAsync(async (req, res, next) => {
    const { id } = req.params
    const { page = 1, size = 1, search = null } = req.query

    const cours = await CoursModel.findByPk(id)
    if (!cours) {
        return next(new ErrorClass(`${id}-ID li cours topilmadi`, 404))
    }

    const students = await StudentModel.findAndCountAll({
        limit: +size,
        offset: (+page - 1) * size,
        where: { coursId: { [Op.eq]: cours.id } }
    }
    )
    students.page = Math.ceil(students.count / +size)


    res.status(200).json({
        status: "succesfull",
        message: `${cours.name} kursida qatnashadigan studentlar ro\`yhati`,
        error: null,
        science: { cours },
        content: students.rows,
        pagination: {
            allPages: students.page,
            allItems: students.count,
            isFirstPage: (+page - 1) === 0,
            isLastPage: students.page === +page,
            page: +page
        }
    })

})

exports.deleteCours = errorAsync(async (req, res, next) => {
    const { id } = req.params
    const coursById = await CoursModel.findByPk(id)
    if (!coursById) {
        return next(new ErrorClass(`${id} id li kurs topilmadi`, 404))
    }
    await coursById.destroy()
    res.status(204).json({
        status: "succes",
        message: "kurs ma`lumotlari o`chirildi",
    })
})