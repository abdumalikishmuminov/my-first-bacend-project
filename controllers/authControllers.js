const { compare } = require("bcryptjs")
const { Op } = require("sequelize")
const { validationResult } = require("express-validator")
const catchAsync = require("../utilts/errorHandlerInAsync")
const UserModel = require("../models/UserModel")
const ErrorClass = require("../utilts/ErrorClass")
const roleManager = require("../utilts/userENUM")
const sendMailFn = require("../utilts/nodemailer")
const generatorToken = require("../utilts/generatorToken")
const emailText = require("../utilts/emailText")

exports.register = catchAsync(async (req, res, next) => {
    const { username, userRole } = req.body
    const validationError = validationResult(req)
    if (!validationError.isEmpty()) {
        let err = new ErrorClass("Cast Error", 400)
        err.errors = validationError.errors
        err.isOperational = true
        return next(err)
    }
    const existSuperAdmin = await UserModel.findOne({ where: { userRole: { [Op.eq]: roleManager.SUPER_ADMIN } } })
    if (existSuperAdmin && userRole === roleManager.SUPER_ADMIN) {
        return next(new ErrorClass(`Faqat bitta ${roleManager.SUPER_ADMIN} ro\`yhatdan o\`tishi mumkin`, 409))
    }
    const existUsername = await UserModel.findOne({ where: { username: { [Op.eq]: username } } })
    if (existUsername) {
        return next(new ErrorClass(`${username} nomli foydalanuvchi ro\`yhatga olingan`, 409))
    }
    // CREATE NEW USER
    const newUser = await UserModel.create(req.body)

    // SEND EMAIL
    sendMailFn(
        newUser.email,
        emailText(newUser.firstName, newUser.verificationCode),
        process.env.EMAIL_SUBJECTTEXT)


    res.status(200).json({
        status: "succesfull",
        message: `Tizimga (username - ${newUser.username} - ${newUser.userRole} ro\`yhatga olindi`
    })
})


exports.verify = catchAsync(async (req, res, next) => {
    const { verifyCode } = req.params
    const condidate = await UserModel.findOne({ where: { verificationCode: { [Op.eq]: verifyCode } } })
    if (condidate.isVerified) {
        return next(new ErrorClass("Siz allaqachon verifkatsiya bo`lgansiz", 409))
    }
    condidate.isVerified = true
    await condidate.save()
    res.status(200).json({
        status: "succesfull",
        message: `${condidate.firstName} verifikatsiyadan muvaffaqiyatli o\`tdi`
    })
})


exports.login = catchAsync(async (req, res, next) => {
    const { username, password } = req.body
    const validationError = validationResult(req)
    if (!validationError.isEmpty()) {
        let err = new ErrorClass("Cast Error", 400)
        err.errors = validationError.errors
        err.isOperational = true
        return next(err)
    }
    const thisThereUsername = await UserModel.findOne({ where: { username: { [Op.eq]: username } } })
    if (!thisThereUsername) {
        return next(new ErrorClass("Foydaluvchi nomi yoki password notog`ri kiritilgan", 403))
    }
    const passwordIsMatchUser = await compare(password, thisThereUsername.password)
    if (!passwordIsMatchUser) {
        return next(new ErrorClass("Foydalanuvchi nomi yoki password noto`gri kiritilgan", 403))
    }

    if(!thisThereUsername.isVerified){
        return next(new ErrorClass("Siz akkauntizni verifikatsiya qilmagansiz, e-mail manzilingizni tekshiring", 403))
    }

    const createToken = await generatorToken({
        id: thisThereUsername.id,
        firstName: thisThereUsername.firstName,
        role: thisThereUsername.userRole,
        phone: thisThereUsername.phoneNumber,
        email: thisThereUsername.email
    }, process.env.JWT_SECRET_CODE, { algorithm: "HS512", expiresIn: "1h" })

    res.status(200).json({
        status: "succesfull",
        message: ` Ismi -  ${thisThereUsername.firstName}, nik nomi - ${thisThereUsername.userRole} tizimga kirdi`,
        content: {
            name: thisThereUsername.firstName,
            username: thisThereUsername.username,
            userRole: thisThereUsername.userRole,
            email: thisThereUsername.email,
            phone: thisThereUsername.phoneNumber
        },
        token: createToken

    })

})