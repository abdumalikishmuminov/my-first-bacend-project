const ErrorClass = require("../utilts/ErrorClass")

const errorHandlerDev = (err, res) => {
    console.log(err.stack)
    res.status(err.statusCode).json({
        status: err.status,
        message: err.message,
        error: err.errors || err.stack
    })
}

const errorProdHandler = (error, res) => {
    console.log("erorProdudHandler ga kirdi")
    res.status(error.statusCode).json({
        status: error.status,
        message: error.message,
        error: error.errors
    })
}

const errorControllers = (err, req, res, next) => {
    console.log(err)
    console.log(err.stack)

    err.statusCode = err.statusCode || 500
    err.status = err.status || "error"

    if (process.env.NODE_ENV === "PROD") {
        console.log("Hello ErrorControllers")
        if (err.isOperational) {
            console.log("bir narsada")
            res.status(err.statusCode).json({
                status: err.status,
                message: err.message
            })
        } else {
            let error = Object.create(err)
            // console.log(error)

            if (error.name === "SequelizeDatabaseError") {
                if (error.original.code === "22P02") {
                    error = new ErrorClass("foydalanivchining darajasi noto'g'ri kiritildi", 400)
                    console.log(error.status)
                    console.log(error.statusCode)
                    console.log(error.message)
                }
            }
            errorProdHandler(error, res)
        }
    } else if (process.env.NODE_ENV === "DEV") {
        errorHandlerDev(err, res)
    }


}

module.exports = errorControllers