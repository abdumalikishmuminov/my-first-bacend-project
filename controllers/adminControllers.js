const { UUIDV4 } = require("sequelize")
const {validationResult} = require("express-validator")
const UserModel = require("../models/UserModel")
const emailText = require("../utilts/emailText")
const ErrorClass = require("../utilts/ErrorClass")
const catchAsync = require("../utilts/errorHandlerInAsync")
const nodemailer = require("../utilts/nodemailer")

exports.getUsers = catchAsync(async (req, res, next) => {
    const {size = 2, page = 1} = req.query
    const allAdmin = UserModel.findAndCountAll({
        limit: size,
        offset: (page-1)*size
    })
    allAdmin.page = Math.ceil(allAdmin.count / size);

    res.status(200).json({
        status: "succesfull",
        message: "Barcha adminlar ro`yhati",
        content: allAdmin.rows,
        pagination: {
            allPages: allAdmin.page,
            totalItems: allAdmin.count,
            isLastPage: allAdmin.page === +page,
            isFirstPage: (+page - 1) === 0,
            hasNextPage: allAdmin.page > +page,
            page: +page
        }
    })
})

exports.createAdmin = catchAsync(async(req,res,next)=>{
    const validationError = validationResult(req)
    if (!validationError.isEmpty()) {
        let err = new ErrorClass("Cast Error", 400)
        err.errors = validationError.errors
        err.isOperational = true
        return next(err)
    }
    const newAdmin = await UserModel.create(req.body)

    res.status(200).json({
        status: "succesfull",
        message: "Yangi admin ro`yhatga olindi",
        error:null,
        content: {newAdmin}
    })
})

exports.updateAdmin = catchAsync(async(req,res,next)=>{
    const { id } = req.params;
    const validationError = validationResult(req)
    if (!validationError.isEmpty()) {
        let err = new ErrorClass("Cast Error", 400)
        err.errors = validationError.errors
        err.isOperational = true
        return next(err)
    };
    const {firstName, lastName, phoneNumber, email, username, password} = req.body

    const searchAdminById = await UserModel.findByPk(id)
    
    if(!searchAdminById){
        return next(new ErrorClass("Bunday foydalanuvchi tizimda mavjud emas", 400))
    }
    searchAdminById.firstName = firstName;
    searchAdminById.lastName = lastName;
    searchAdminById.email = email;
    searchAdminById.phoneNumber = phoneNumber;
    searchAdminById.username = username;
    searchAdminById.password = password;
    searchAdminById.isVerified = false;
    searchAdminById.verificationCode = UUIDV4
    await searchAdminById.save()
    
    nodemailer(
        email,
        emailText(searchAdminById.firstName,searchAdminById.verificationCode),
        process.env.EMAIL_SUBJECTTEXT
        )

    res.status(200).json({
        status: "succesfull",
        message: "Admin ma`lumotlari taxrirlandi",
        error: null,
        content: null
    })
})

exports.deleteAdmin = catchAsync(async(req,res,next)=>{
    const {id} = req.params;
    const admin = await UserModel.findByPk(id)
    if(!admin){
        return next(new ErrorClass("Bunday admin tizimda mavjud emas", 404))
    }
    await admin.destroy()

    res.status(200).json({
        status: "succesfull",
        message: "Admin tizimdan o`chirildi",
        error: null,
        data: null
    })
})